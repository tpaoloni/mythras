# Foundry VTT Mythras

An implementation of the Mythras game system (http://thedesignmechanism.com) for Foundry Virtual Tabletop (http://foundryvtt.com).

This product references the Mythras Imperative rules, available from The Design Mechanism at www.thedesignmechanism.com and all associated logos and trademarks are copyrights of The Design Mechanism. Used with permission. The Design Mechanism makes no representation or warranty as to the quality, viability, or suitability for purpose of this product.

<img src="http://thedesignmechanism.com/resources/mythras-gateway-logo-black-small.jpg" alt="Mythras Gateway" width="200" />

## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md)
